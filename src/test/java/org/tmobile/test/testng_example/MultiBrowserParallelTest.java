package org.tmobile.test.testng_example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

public class MultiBrowserParallelTest {

	WebDriver driver;

	/*
	 * @BeforeMethod public void beforeMethod() { driver = new ChromeDriver(); }
	 */
	@Test
	public void testMethodsOne() {
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares" + "\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.amazon.com/");
		// WebElement ele = driver.findElement(By.id("twotabsearchtextbox"));

		// ele.sendKeys("siddhartha herman hesse");
		// WebElement go = driver.findElement(By.id("nav-search-submit-text"));
		// go.click();

	}

	@Test
	public void testMethodsTwo() {
		System.setProperty("webdriver.gecko.driver", "C:\\Softwares\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://app.schoology.com/login");
		/*
		 * WebElement ele = driver.findElement(By.id("lst-ib"));
		 * ele.sendKeys("siddhartha herman hesse"); WebElement go =
		 * driver.findElement(By.name("btnK")); go.click();
		 */

	}

	@Test
	public void testMethodsThree() {
		System.setProperty("webdriver.ie.driver", "C:\\Softwares\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		driver.get("https://www.google.com/");
		/*WebElement ele = driver.findElement(By.id("uh-search-box"));
		ele.sendKeys("siddhartha herman hesse");
		WebElement go = driver.findElement(By.id("uh-search-button"));
		go.click();*/

	}

	/*
	 * @AfterMethod public void afterMethod() { driver.close(); }
	 */

}