package org.tmobile.test.testng_example;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ParallelChromeTest {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		 driver = new ChromeDriver();
	}

	@Test
	public void testMethodsOne() {
		driver.get("https://www.amazon.com/");
		//WebElement elementOne = driver.findElement(By.id("twotabsearchtextbox"));

		//elementOne.sendKeys("siddhartha herman hesse");
		//elementOne.sendKeys(Keys.ENTER);

	}

	@Test
	public void testMethodsTwo() {
		driver.get("https://www.google.com/");
		//WebElement elementTwo = driver.findElement(By.name("q"));

		//elementTwo.sendKeys("siddhartha herman hesse");
		//elementTwo.sendKeys(Keys.ENTER);
		
	}

	@Test
	public void testMethodsThree() {
		driver.get("https://www.yahoo.com/");
		//WebElement elementThree = driver.findElement(By.name("p"));
		//elementThree.sendKeys("siddhartha herman hesse");
		//elementThree.sendKeys(Keys.ENTER);
		
	}

	@AfterMethod
	public void afterMethod() {
		// driver.close();
		
	}

}